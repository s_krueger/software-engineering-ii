package tests.tiere;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import tiere.Schwein;

public class SchweinTest {
    private Schwein objectUnderTest;

    // Setup-Funktion, die vor jedem Test das objectUnderTest aus der Klasse Schwein neu instanziiert
    @BeforeEach
    public void setup() {
        objectUnderTest = new Schwein();
    }

    // Testet den Default-Konstruktor
    @Test
    public void constructor_Default_Success() {
        assertEquals("nobody", objectUnderTest.getName());
        assertEquals(10, objectUnderTest.getGewicht());
    }

    // Testet den Konstruktor mit Namen
    @Test
    public void constructor_InstantiateWithName_Success() {
        String name = "Gertrud";
        objectUnderTest = new Schwein(name);

        assertEquals(name, objectUnderTest.getName());
        assertEquals(10, objectUnderTest.getGewicht());
    }

    // Testet die Konstruktor-Exception bei Null-Namen
    @Test
    public void constructor_InstantiateWithNullName_IllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> {
            objectUnderTest = new Schwein(null);
        });
    }

    // Testet die Konstruktor-Exception bei ungültigem Namen
    @Test
    public void constructor_InstantiateWithIllegalName_IllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> {
            objectUnderTest = new Schwein("Elsa");
        });
    }

    // Testet die setName-Methode
    @Test
    public void setName_ValidName_Success() {
        objectUnderTest.setName("Gertrud");

        assertEquals("Gertrud", objectUnderTest.getName());
    }

    // Testet die setName-Exception bei Null-Namen
    @Test
    public void setName_NullName_IllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> {
            objectUnderTest.setName(null);
        });
    }

    // Testet die setName-Exception bei ungültigem Namen
    @Test
    public void setName_IllegalName_IllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> {
            objectUnderTest.setName("Elsa");
        });
    }

    // Testet die fressen-Methode
    @Test
    public void fressen_NoArguments_Success() {
        objectUnderTest.fressen();

        assertEquals(11, objectUnderTest.getGewicht());
    }
}